#include "monaghan_wave.h"

static double found_next_gvelocity(double prev_gvel, double prev_grho, double prev_gcoord, double gmass, double dmass,
                                   double * image_prev_grho, double * image_prev_gcoord, double * image_prev_drho,
                                   double * image_prev_dvel,  double * image_prev_dcoord, int image_gamount,
                                   int image_damount, ProblemParams params)
{
    double a_p = 0;
    double drag = 0;
    double result = 0;
    double r_ja = 0;

    for(int j = 0; j < image_gamount; ++j)
    {
        if (fabs(prev_gcoord - image_prev_gcoord[j]) <= 2. * params.h)
        {
            a_p += ((1. / prev_grho) + 1. / image_prev_grho[j])
                   * spline_gradient(prev_gcoord, image_prev_gcoord[j], params);
        }
    }
    a_p *= params.c_s * params.c_s * gmass;

    for(int j = 0; j < image_damount; ++j)
    {
        if (fabs(prev_gcoord - image_prev_dcoord[j]) <= 2. * params.h)
        {
            r_ja = image_prev_dcoord[j] - prev_gcoord;
            drag += ((prev_gvel - image_prev_dvel[j]) * r_ja / (pow(r_ja, 2) + 0.001 * params.h * params.h) /
                     prev_grho /
                     image_prev_drho[j] * r_ja) * spline_kernel(prev_gcoord, image_prev_dcoord[j], params);
        }
    }
    drag = drag * dmass * params.K;

    result = - params.tau * a_p - params.tau * drag + prev_gvel;

    return result;
}

static double found_next_dvelocity(double prev_dvel, double prev_drho, double prev_dcoord, double gmass,
                                   double * image_prev_gvel, double * image_prev_grho, double * image_prev_gcoord,
                                   int image_gamount, ProblemParams params)
{
    double result = 0;
    double r_ja = 0;
    for(int j = 0; j < image_gamount; ++j)
    {
        if (fabs(prev_dcoord - image_prev_gcoord[j]) <= 2. * params.h)
        {
            r_ja = prev_dcoord - image_prev_gcoord[j];
            result += ((image_prev_gvel[j] - prev_dvel) * r_ja / (pow(r_ja, 2) + 0.001 * params.h * params.h) /
                       prev_drho / image_prev_grho[j] ) * r_ja * spline_kernel(prev_dcoord, image_prev_gcoord[j], params);
        }
    }

    return params.tau * gmass * params.K * result + prev_dvel;
}

void monaghan_wave(ParticleParams gas_params, ParticleParams dust_params, ProblemParams problem_params)
{
    int gamount = gas_params.amount;
    int damount = dust_params.amount;

    double T = problem_params.T;
    double tau = problem_params.tau;

    // Initialize arrays for gas. BEGIN
    size_t gas_array_size = gamount * sizeof(double);

    double* prev_x_g = malloc(gas_array_size);
    double* next_x_g = malloc(gas_array_size);

    double* prev_gvelocity = malloc(gas_array_size);
    double* next_gvelocity = malloc(gas_array_size);
    double* prev_grho = malloc(gas_array_size);
    double* next_grho = malloc(gas_array_size);

    int gamount3 = 3 * gamount;
    int gas_array_size3 = gamount3 * sizeof(double);
    double* prev_image_x_g = malloc(gas_array_size3);
    double* next_image_x_g = malloc(gas_array_size3);

    double* prev_image_gvelocity = malloc(gas_array_size3);
    double* next_image_gvelocity = malloc(gas_array_size3);
    double* prev_image_grho = malloc(gas_array_size3);
    double* next_image_grho = malloc(gas_array_size3);

    double gmass = compute_mass(gas_params, problem_params);

    for(int i = 0; i < gamount; ++i)
    {
        prev_x_g[i] = NAN;
        next_x_g[i] = NAN;
        prev_gvelocity[i] = NAN;
        next_gvelocity[i] = NAN;
        prev_grho[i] = NAN;
        next_grho[i] = NAN;
    }

    for(int j = 0; j < 3 * gamount; ++j)
    {
        prev_image_x_g[j] = NAN;
        next_image_x_g[j] = NAN;
        prev_image_gvelocity[j] = NAN;
        next_image_gvelocity[j] = NAN;
        prev_image_grho[j] = NAN;
        next_image_grho[j] = NAN;
    }

    fill_x(prev_x_g, problem_params, gas_params);
    fill_image_x(prev_image_x_g, prev_x_g, gas_params);

    fill_initial_rho(prev_grho, gmass, prev_x_g, prev_image_x_g, gas_params, problem_params);
    fill_initial_gvelocity(prev_gvelocity, prev_x_g, gas_params, problem_params);

    fill_image(prev_image_grho, prev_grho, gas_params);
    fill_image(prev_image_gvelocity, prev_gvelocity, gas_params);
    // Initialize arrays for gas. END

    // Initialize arrays for dust. BEGIN
    size_t dust_array_size = damount * sizeof(double);
    double* prev_x_d = malloc(dust_array_size);
    double* next_x_d = malloc(dust_array_size);

    double* prev_dvelocity = malloc(dust_array_size);
    double* next_dvelocity = malloc(dust_array_size);
    double* prev_drho = malloc(dust_array_size);
    double* next_drho = malloc(dust_array_size);

    int damount3 = 3 * damount;
    size_t dust_array_size3 = damount3 * sizeof(double);
    double* prev_image_x_d = malloc(dust_array_size3);
    double* next_image_x_d = malloc(dust_array_size3);

    double* prev_image_dvelocity = malloc(dust_array_size3);
    double* next_image_dvelocity = malloc(dust_array_size3);
    double* prev_image_drho = malloc(dust_array_size3);
    double* next_image_drho = malloc(dust_array_size3);

    double dmass = compute_mass(dust_params, problem_params);

    for(int i = 0; i < damount; ++i)
    {
        prev_x_d[i] = NAN;
        next_x_d[i] = NAN;
        prev_dvelocity[i] = NAN;
        next_dvelocity[i] = NAN;
        prev_drho[i] = NAN;
        next_drho[i] = NAN;
    }

    for(int j = 0; j < 3 * damount; ++j)
    {
        prev_image_x_d[j] = NAN;
        next_image_x_d[j] = NAN;
        prev_image_dvelocity[j] = NAN;
        next_image_dvelocity[j] = NAN;
        prev_image_drho[j] = NAN;
        next_image_drho[j] = NAN;
    }

    fill_x(prev_x_d, problem_params, dust_params);
    fill_image_x(prev_image_x_d, prev_x_d, dust_params);

    fill_initial_rho(prev_drho, dmass, prev_x_d, prev_image_x_d, dust_params, problem_params);
    fill_initial_dvelocity(prev_dvelocity, prev_x_d, dust_params, problem_params);

    fill_image(prev_image_drho, prev_drho, dust_params);
    fill_image(prev_image_dvelocity, prev_dvelocity, dust_params);
    // Initialize arrays for dust. END

    char fileName[512];

    /*
    sprintf(fileName, "%s/monaghan_gas_d2g=%lg_h=%lg_tau=%lg_T=0_K=%lg.dat", DATA_DIR,
            problem_params.d2g, problem_params.h, problem_params.tau, problem_params.T, problem_params.K);

    FILE * explicit_gas_0 = fopen(fileName, "w");
    for (int j = 0; j < gamount; ++j)
    {
        fprintf(explicit_gas_0, "%lf %lf %lf\n", prev_x_g[j], prev_gvelocity[j], prev_grho[j]);
    }
    fclose(explicit_gas_0);

    sprintf(fileName, "%s/monaghan_dust_d2g=%lg_h=%lg_tau=%lg_T=0_K=%lg.dat", DATA_DIR,
            problem_params.d2g, problem_params.h, problem_params.tau, problem_params.T, problem_params.K);
    FILE * explicit_dust_0 = fopen(fileName, "w");
    for (int j = 0; j < damount; ++j)
    {
        fprintf(explicit_dust_0, "%lf %lf %lf\n", prev_x_d[j], prev_dvelocity[j], prev_drho[j]);
    }
    fclose(explicit_dust_0);
*/

    for (int frameId = 0; frameId < floor(T / tau); ++frameId)
    {
        printf("%d\n", frameId);

        for(int i = 0; i < gamount; ++i)
        {
            next_gvelocity[i] = found_next_gvelocity(prev_gvelocity[i], prev_grho[i], prev_x_g[i], gmass, dmass,
                                                     prev_image_grho, prev_image_x_g, prev_image_drho,
                                                     prev_image_dvelocity, prev_image_x_d, 3 * gamount, 3 * damount,
                                                     problem_params);
            assert(!isnan(next_gvelocity[i]));
            next_x_g[i] = next_coordinate(prev_x_g[i], prev_gvelocity[i], problem_params);
        }

        for(int i = 0; i < damount; ++i)
        {
            next_dvelocity[i] = found_next_dvelocity(prev_dvelocity[i], prev_drho[i], prev_x_d[i], gmass,
                                                     prev_image_gvelocity, prev_image_grho, prev_image_x_g, 3 * gamount,
                                                     problem_params);
            assert(!isnan(next_dvelocity[i]));
            next_x_d[i] = next_coordinate(prev_x_d[i], prev_dvelocity[i], problem_params);
        }

        fill_image(next_image_gvelocity, next_gvelocity, gas_params);
        fill_image(next_image_dvelocity, next_dvelocity, dust_params);

        for (int i = 0; i < 3 * gamount; ++i)
        {
            next_image_x_g[i] = next_coordinate(prev_image_x_g[i], prev_image_gvelocity[i], problem_params);
            assert(!isnan(next_image_x_g[i]));
        }

        for (int i = 0; i < 3 * damount; ++i)
        {
            next_image_x_d[i] = next_coordinate(prev_image_x_d[i], prev_image_dvelocity[i], problem_params);
            assert(!isnan(next_image_x_d[i]));
        }
        for(int i = 0; i < gamount; ++i)
        {
            next_grho[i] = next_rho(gmass, next_x_g, next_image_x_g, i, gas_params, problem_params);
        }
        for(int i = 0; i < damount; ++i)
        {
            next_drho[i] = next_rho(dmass, next_x_d, next_image_x_d, i, dust_params, problem_params);
        }
        fill_image(next_image_grho, next_grho, gas_params);
        fill_image(next_image_drho, next_drho, dust_params);

        for(int i = 0; i < gamount; ++i)
        {
            prev_grho[i] = next_grho[i];
            prev_gvelocity[i] = next_gvelocity[i];
            prev_x_g[i] = next_x_g[i];
        }

        for(int i = 0; i < damount; ++i)
        {
            prev_drho[i] = next_drho[i];
            prev_dvelocity[i] = next_dvelocity[i];
            prev_x_d[i] = next_x_d[i];
        }

        for(int i = 0; i < 3 * gamount; ++i)
        {
            prev_image_grho[i] = next_image_grho[i];
            prev_image_gvelocity[i] = next_image_gvelocity[i];
            prev_image_x_g[i] = next_image_x_g[i];

        }

        for(int i = 0; i < 3 * damount; ++i)
        {
            prev_image_drho[i] = next_image_drho[i];
            prev_image_dvelocity[i] = next_image_dvelocity[i];
            prev_image_x_d[i] = next_image_x_d[i];
        }
    }

    sprintf(fileName, "%s%smonaghan_gas_T=%lg_d2g=%lg_h=%lg_tau=%lg_K=%lg_N=%d.dat", DATA_DIR, DIR_SEPARATOR,
            problem_params.T, problem_params.d2g, problem_params.h, problem_params.tau, problem_params.K,
            gas_params.amount);
    FILE * monaghan_gas_T = fopen(fileName, "w");
    for (int j = 0; j < gamount; ++j)
    {
        fprintf(monaghan_gas_T, "%lf %lf %lf\n", prev_x_g[j], prev_gvelocity[j], prev_grho[j]);
    }
    fclose(monaghan_gas_T);

    sprintf(fileName, "%s%smonaghan_dust_T=%lg_d2g=%lg_h=%lg_tau=%lg_K=%lg_N=%d.dat", DATA_DIR, DIR_SEPARATOR,
            problem_params.T, problem_params.d2g, problem_params.h, problem_params.tau, problem_params.K,
            dust_params.amount);
    FILE * monaghan_dust_T = fopen(fileName, "w");
    for (int j = 0; j < damount; ++j)
    {
        fprintf(monaghan_dust_T, "%lf %lf %lf\n", prev_x_d[j], prev_dvelocity[j], prev_drho[j]);
    }
    fclose(monaghan_dust_T);

    printf("----");

    // Free allocated arrays
    free(prev_x_g);
    free(next_x_g);

    free(prev_gvelocity);
    free(next_gvelocity);
    free(prev_grho);
    free(next_grho);

    free(prev_image_x_g);
    free(next_image_x_g);

    free(prev_image_gvelocity);
    free(next_image_gvelocity);
    free(prev_image_grho);
    free(next_image_grho);

    free(prev_x_d);
    free(next_x_d);

    free(prev_dvelocity);
    free(next_dvelocity);
    free(prev_drho);
    free(next_drho);

    free(prev_image_x_d);
    free(next_image_x_d);

    free(prev_image_dvelocity);
    free(next_image_dvelocity);
    free(prev_image_drho);
    free(next_image_drho);
}