#include "explicit.h"

static double found_next_gvelocity(double x_g, double prev_grho, double prev_gvelocity, double prev_dvelocity,
                            double gmass, double * prev_image_x_g, double * prev_image_grho, int i,
                            ParticleParams gas_params, ProblemParams problem_params)
{
    int amount = gas_params.amount;
    double c_s = problem_params.c_s;
    double tau = problem_params.tau;
    double K = problem_params.K;
    double d2g = problem_params.d2g;
    double t_stop = problem_params.t_stop;

    double result = 0;

    for (int j = 0; j < 3 * amount - 2; ++j)
    {
        if (fabs(x_g - prev_image_x_g[j]) < 2.1 * problem_params.h)
        {
            result += (1. / prev_image_grho[j] + 1. / prev_grho) *
                      spline_gradient(x_g, prev_image_x_g[j], problem_params);
        }
    }
    result = -tau * gmass * result * c_s * c_s;
    result = result - tau * d2g / t_stop * (prev_gvelocity - prev_dvelocity) + prev_gvelocity;
    return result;
}

static double found_next_dvelocity(double prev_drho, double prev_dvelocity, double prev_gvelocity, ProblemParams params)
{
    double tau = params.tau;
    double K = params.K;
    double t_stop = params.t_stop;

    double result = 0;

    result = tau / t_stop * (prev_gvelocity - prev_dvelocity) + prev_dvelocity;
    return result;
}

void explicit_scheme(ParticleParams gas_params, ParticleParams dust_params, ProblemParams problem_params)
{
    int gamount = gas_params.amount;
    int damount = dust_params.amount;

    double T = problem_params.T;
    double tau = problem_params.tau;

    // Initialize arrays for gas. BEGIN
    size_t gas_array_size = gamount * sizeof(double);
    double* prev_x_g = malloc(gas_array_size);
    double* next_x_g = malloc(gas_array_size);

    double* prev_gvelocity = malloc(gas_array_size);
    double* next_gvelocity = malloc(gas_array_size);
    double* prev_grho = malloc(gas_array_size);
    double* next_grho = malloc(gas_array_size);

    int gamount3 = 3 * gamount;
    int gas_array_size3 = gamount3 * sizeof(double);
    double* prev_image_x_g = malloc(gas_array_size3);
    double* next_image_x_g = malloc(gas_array_size3);

    double* prev_image_gvelocity = malloc(gas_array_size3);
    double* next_image_gvelocity = malloc(gas_array_size3);
    double* prev_image_grho = malloc(gas_array_size3);
    double* next_image_grho = malloc(gas_array_size3);

    double gmass = compute_mass(gas_params, problem_params);

    for(int i = 0; i < gamount; ++i)
    {
        prev_x_g[i] = NAN;
        next_x_g[i] = NAN;
        prev_gvelocity[i] = NAN;
        next_gvelocity[i] = NAN;
        prev_grho[i] = NAN;
        next_grho[i] = NAN;
    }

    for(int j = 0; j < gamount3; ++j)
    {
        prev_image_x_g[j] = NAN;
        next_image_x_g[j] = NAN;
        prev_image_gvelocity[j] = NAN;
        next_image_gvelocity[j] = NAN;
        prev_image_grho[j] = NAN;
        next_image_grho[j] = NAN;
    }

    fill_x(prev_x_g, problem_params, gas_params);
    fill_image_x(prev_image_x_g, prev_x_g, gas_params);

    fill_initial_rho(prev_grho, gmass, prev_x_g, prev_image_x_g, gas_params, problem_params);
    fill_initial_gvelocity(prev_gvelocity, prev_x_g, gas_params, problem_params);

    fill_image(prev_image_grho, prev_grho, gas_params);
    fill_image(prev_image_gvelocity, prev_gvelocity, gas_params);
    // Initialize arrays for gas. END

    // Initialize arrays for dust. BEGIN
    size_t dust_array_size = damount * sizeof(double);
    double* prev_x_d = malloc(dust_array_size);
    double* next_x_d = malloc(dust_array_size);

    double* prev_dvelocity = malloc(dust_array_size);
    double* next_dvelocity = malloc(dust_array_size);
    double* prev_drho = malloc(dust_array_size);
    double* next_drho = malloc(dust_array_size);

    int damount3 = 3 * damount;
    size_t dust_array_size3 = damount3 * sizeof(double);
    double* prev_image_x_d = malloc(dust_array_size3);
    double* next_image_x_d = malloc(dust_array_size3);

    double* prev_image_dvelocity = malloc(dust_array_size3);
    double* next_image_dvelocity = malloc(dust_array_size3);
    double* prev_image_drho = malloc(dust_array_size3);
    double* next_image_drho = malloc(dust_array_size3);

    double dmass = compute_mass(dust_params, problem_params);

    for(int i = 0; i < damount; ++i)
    {
        prev_x_d[i] = NAN;
        next_x_d[i] = NAN;
        prev_dvelocity[i] = NAN;
        next_dvelocity[i] = NAN;
        prev_drho[i] = NAN;
        next_drho[i] = NAN;
    }

    for(int j = 0; j < damount3; ++j)
    {
        prev_image_x_d[j] = NAN;
        next_image_x_d[j] = NAN;
        prev_image_dvelocity[j] = NAN;
        next_image_dvelocity[j] = NAN;
        prev_image_drho[j] = NAN;
        next_image_drho[j] = NAN;
    }

    fill_x(prev_x_d, problem_params, dust_params);
    fill_image_x(prev_image_x_d, prev_x_d, dust_params);

    fill_initial_rho(prev_drho, dmass, prev_x_d, prev_image_x_d, dust_params, problem_params);
    fill_initial_dvelocity(prev_dvelocity, prev_x_d, dust_params, problem_params);

    fill_image(prev_image_drho, prev_drho, dust_params);
    fill_image(prev_image_dvelocity, prev_dvelocity, dust_params);
    // Initialize arrays for dust. END

    // dust velocity at gas points
    double* prev_dvel_xg = malloc(gas_array_size);
    // gas velocity at dust points
    double* prev_gvel_xd = malloc(dust_array_size);

    for(int i  = 0; i < gamount; ++i)
    {
        prev_dvel_xg[i] = NAN;
    }
    for(int i = 0; i < damount; ++i)
    {
        prev_gvel_xd[i] = NAN;
    }

    char fileName[512];

    sprintf(fileName, "%s%sexplicit_gas_T=0_d2g=%lg_h=%lg_tau=%lg_K=%lg.dat", DATA_DIR, DIR_SEPARATOR,
            problem_params.d2g, problem_params.h, problem_params.tau, problem_params.K);

    FILE * explicit_gas_0 = fopen(fileName, "w");
    for (int j = 0; j < gamount; ++j)
    {
        fprintf(explicit_gas_0, "%lf %lf %lf\n", prev_x_g[j], prev_gvelocity[j], prev_grho[j]);
    }
    fclose(explicit_gas_0);

    sprintf(fileName, "%s%sexplicit_dust_T=0_d2g=%lg_h=%lg_tau=%lg_K=%lg.dat", DATA_DIR, DIR_SEPARATOR,
            problem_params.d2g, problem_params.h, problem_params.tau, problem_params.K);
    FILE * explicit_dust_0 = fopen(fileName, "w");
    for (int j = 0; j < damount; ++j)
    {
        fprintf(explicit_dust_0, "%lf %lf %lf\n", prev_x_d[j], prev_dvelocity[j], prev_drho[j]);
    }
    fclose(explicit_dust_0);


    for (int frameId = 0; frameId < floor(T / tau); ++frameId)
    {
        printf("%d\n", frameId);

        for(int i = 0; i < gamount; ++i)
        {
            prev_dvel_xg[i] = interpolation_value(prev_x_g[i], prev_image_dvelocity, dmass, prev_image_drho,
                                                  prev_image_x_d, dust_params, problem_params);
            assert(!isnan(prev_dvel_xg[i]));
        }
        for(int i = 0; i < damount; ++i)
        {
            prev_gvel_xd[i] = interpolation_value(prev_x_d[i], prev_image_gvelocity, gmass, prev_image_grho,
                                                  prev_image_x_g, gas_params, problem_params);
            assert(!isnan(prev_gvel_xd[i]));
        }

        for(int i = 0; i < gamount; ++i)
        {
            next_gvelocity[i] = found_next_gvelocity(prev_x_g[i], prev_grho[i], prev_gvelocity[i], prev_dvel_xg[i], gmass,
                                                     prev_image_x_g, prev_image_grho, i, gas_params, problem_params);
            assert(!isnan(next_gvelocity[i]));
            next_x_g[i] = next_coordinate(prev_x_g[i], prev_gvelocity[i], problem_params);
        }

        for(int i = 0; i < damount; ++i)
        {
            next_dvelocity[i] = found_next_dvelocity(prev_drho[i], prev_dvelocity[i], prev_gvel_xd[i], problem_params);
            assert(!isnan(next_dvelocity[i]));
            next_x_d[i] = next_coordinate(prev_x_d[i], prev_dvelocity[i], problem_params);
        }

        fill_image(next_image_gvelocity, next_gvelocity, gas_params);

        fill_image(next_image_dvelocity, next_dvelocity, dust_params);

        for (int i = 0; i < 3 * gamount - 2; ++i)
        {
            next_image_x_g[i] = next_coordinate(prev_image_x_g[i], prev_image_gvelocity[i], problem_params);
            assert(!isnan(next_image_x_g[i]));
        }

        for (int i = 0; i < 3 * damount - 2; ++i)
        {
            next_image_x_d[i] = next_coordinate(prev_image_x_d[i], prev_image_dvelocity[i], problem_params);
            assert(!isnan(next_image_x_d[i]));
        }
        for(int i = 0; i < gamount; ++i)
        {
            next_grho[i] = next_rho(gmass, next_x_g, next_image_x_g, i, gas_params, problem_params);
        }
        for(int i = 0; i < damount; ++i)
        {
            next_drho[i] = next_rho(dmass, next_x_d, next_image_x_d, i, dust_params, problem_params);
        }
        fill_image(next_image_grho, next_grho, gas_params);
        fill_image(next_image_drho, next_drho, dust_params);

        for(int i = 0; i < gamount; ++i)
        {
            prev_grho[i] = next_grho[i];
            prev_gvelocity[i] = next_gvelocity[i];
            prev_x_g[i] = next_x_g[i];
        }

        for(int i = 0; i < damount; ++i)
        {
            prev_drho[i] = next_drho[i];
            prev_dvelocity[i] = next_dvelocity[i];
            prev_x_d[i] = next_x_d[i];
        }

        for(int i = 0; i < 3 * gamount - 2; ++i)
        {
            prev_image_grho[i] = next_image_grho[i];
            prev_image_gvelocity[i] = next_image_gvelocity[i];
            prev_image_x_g[i] = next_image_x_g[i];

        }

        for(int i = 0; i < 3 * damount - 2; ++i)
        {
            prev_image_drho[i] = next_image_drho[i];
            prev_image_dvelocity[i] = next_image_dvelocity[i];
            prev_image_x_d[i] = next_image_x_d[i];

        }
    }

    sprintf(fileName, "%s%sexplicit_gas_T=%lg_d2g=%lg_h=%lg_tau=%lg_K=%lg.dat", DATA_DIR, DIR_SEPARATOR,
            problem_params.T, problem_params.d2g, problem_params.h, problem_params.tau, problem_params.K);
    FILE * explicit_gas_T = fopen(fileName, "w");
    for (int j = 0; j < gamount; ++j)
    {
        fprintf(explicit_gas_T, "%lf %lf %lf\n", prev_x_g[j], prev_gvelocity[j], prev_grho[j]);
    }
    fclose(explicit_gas_T);
    sprintf(fileName, "%s%sexplicit_dust_T=%lg_d2g=%lg_h=%lg_tau=%lg_K=%lg.dat", DATA_DIR, DIR_SEPARATOR,
            problem_params.T, problem_params.d2g, problem_params.h, problem_params.tau, problem_params.K);
    FILE * explicit_dust_T = fopen(fileName, "w");
    for (int j = 0; j < damount; ++j)
    {
        fprintf(explicit_dust_T, "%lf %lf %lf\n", prev_x_d[j], prev_dvelocity[j], prev_drho[j]);
    }
    fclose(explicit_dust_T);

    printf("----");

    // Free allocated arrays
    free(prev_x_g);
    free(next_x_g);

    free(prev_gvelocity);
    free(next_gvelocity);
    free(prev_grho);
    free(next_grho);

    free(prev_image_x_g);
    free(next_image_x_g);

    free(prev_image_gvelocity);
    free(next_image_gvelocity);
    free(prev_image_grho);
    free(next_image_grho);


    free(prev_x_d);
    free(next_x_d);

    free(prev_dvelocity);
    free(next_dvelocity);
    free(prev_drho);
    free(next_drho);

    free(prev_image_x_d);
    free(next_image_x_d);

    free(prev_image_dvelocity);
    free(next_image_dvelocity);
    free(prev_image_drho);
    free(next_image_drho);

    free(prev_dvel_xg);
    free(prev_gvel_xd);
}