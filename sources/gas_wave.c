#include "gas_wave.h"

//начальное распределение скорости газа
double gvelocity_distribution(double x, ProblemParams params)
{
    return params.delta * sin(2.*pi*x);
}

void fill_initial_gvelocity(double * gvelocity, double * x_g, ParticleParams particleParams, ProblemParams problemParams)
{
    for (int i = 0; i < particleParams.amount; ++i)
    {
        gvelocity[i] = gvelocity_distribution(x_g[i], problemParams);
    }
}
