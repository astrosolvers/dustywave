#include "dust_wave.h"

//начальное распределение скорости пыли
double dvelocity_distribution(double x, ProblemParams params)
{
    return params.delta * sin(2.*pi*x);
}

void fill_initial_dvelocity(double * dvelocity, double * x_d, ParticleParams particleParams, ProblemParams problemParams)
{
    for (int i = 0; i < particleParams.amount; ++i)
    {
        dvelocity[i] = dvelocity_distribution(x_d[i], problemParams);
    }
}
