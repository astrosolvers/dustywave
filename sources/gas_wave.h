#pragma once

#include <stdio.h>
#include <math.h>
#include <malloc.h>
#include <stdlib.h>

#include "common_use.h"

//начальное распределение плотности газа
double gdensity_distribution(double x, ProblemParams params);

void fill_initial_gvelocity(double * gvelocity, double * x_g, ParticleParams particleParams,
                            ProblemParams problemParams);
