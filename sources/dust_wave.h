#pragma once

#include <stdio.h>
#include <math.h>
#include <malloc.h>
#include <stdlib.h>

#include "common_use.h"

//начальное распределение плотности пыли
double ddensity_distribution(double x, ProblemParams params);

void fill_initial_dvelocity(double * dvelocity, double * x_d, ParticleParams particleParams,
                            ProblemParams problemParams);
