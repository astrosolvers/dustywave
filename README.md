1. Run one of the scripts:

(Cmake is required)

  OS Windows:
      run_cmake_win.cmd
    It produces a solution for Visual Studio into ./build directory. 
    Open the solution and build test app. 

  OS Linux:
      run_cmake_linux.sh
    It produces a makefile in ./build directory and runs 'make'

The resulting executable will be placed into ./bin directory.

2. Modify ./bin/problem_params.txt to the desired values 
  (written in the format: delta, d2g, K, h, tau):
0.0001 0.01 0.01 0.05 0.005

Other parameters could be set directly in 'main.c' file as hard-coded values
(see initialization of structures: problem_params, gw_particle, dw_particle)

3. Run executable

4. The output will be written to the ./bin/particles directory.

