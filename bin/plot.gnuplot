set term pngcairo size 1800,1800 color font 'Times Roman, 24'
set output './particles/differ/tmp.png'
set style line 1 lt 1 lw 6 lc rgb 'red' pt -1
set style line 2 lt 1 lw 3 lc rgb 'black' pt 3 ps 1
set style line 3 lt 1 lw 3 lc rgb 'magenta ' pt 3 ps 1
set style line 4 lt 1 lw 4 lc rgb 'blue' pt 3 ps 1
set style line 5 lt 1 lw 2 lc rgb 'black' pt 3 ps 1
set macros
# MACROS
TMARGIN = "set tmargin at screen 0.92; set bmargin at screen 0.66"
MMARGIN = "set tmargin at screen 0.64; set bmargin at screen 0.38"
BMARGIN = "set tmargin at screen 0.36; set bmargin at screen 0.1"
LMARGIN = "set lmargin at screen 0.15; set rmargin at screen 0.49"
RMARGIN = "set lmargin at screen 0.51; set rmargin at screen 0.85"
set multiplot layout 2,2
set key left top
set border ls 5
set x2label 'Distance'
set x2tics
set ylabel 'Dust velocity'
set mytics 2 
#set label 1 'explicit, tau=0.0001' at graph 0.4,0.1
set autoscale y
#set yrange [-0.01:0.01]
#set y2range [0.985:1.015]
set xrange [0:1]
set x2range [0:1]
set mx2tics 
#1
set x2tics mirror
unset xtics
@TMARGIN; @LMARGIN
plot './particles/differ/cells_dust_T=0.5_d2g=0.01_h=0.05_tau=0.005_K=0.01.dat' using 1:2 title "cells" w l ls 3, '/home/calat/CLionProjects/particles/differ/explicit_dust_T=0.5_d2g=0.01_h=0.05_tau=0.005_K=0.01.dat' using 1:2 title "explicit" w l ls 4

set key right top
#set label 2 'explicit' at graph 0.5,0.1
set y2label 'Gas density'
set y2tics
set my2tics 2 
unset ylabel
unset ytics
set y2tics mirror
@TMARGIN; @RMARGIN
plot './particles/differ/cells_gas_T=0.5_d2g=0.01_h=0.05_tau=0.005_K=0.01.dat' using 1:2 title "cells" w l ls 3, '/home/calat/CLionProjects/particles/differ/explicit_gas_T=0.5_d2g=0.01_h=0.05_tau=0.005_K=0.01.dat' using 1:2 title "explicit" w l ls 4

unset label 1
unset label 2
unset x2label
unset x2tics
#set label 1 'smooth, CFL=0.1' at graph 0.4,0.1
set key left top
set mxtics 2
#set xtics
unset y2label 
unset y2tics
set mytics 2 
set mxtics 1
set ytics
#set ylabel 'Dust velocity'

set format x ""
set xtics
set mxtics

@MMARGIN; @LMARGIN
plot './particles/differ/cells_dust_T=0.5_d2g=0.01_h=0.05_tau=0.005_K=0.01.dat' using 1:3 title "cells" w l ls 3, '/home/calat/CLionProjects/particles/differ/explicit_dust_T=0.5_d2g=0.01_h=0.05_tau=0.005_K=0.01.dat' using 1:3 title "explicit" w l ls 4


#set y2label 'Gas density'
set key left top
#set label 2 'smooth' at graph 0.5,0.1
set y2tics

unset ylabel
unset ytics
set y2tics mirror

@MMARGIN; @RMARGIN
#set key left bottom
plot './particles/differ/cells_gas_T=0.5_d2g=0.01_h=0.05_tau=0.005_K=0.01.dat' using 1:3 title "cells" w l ls 3, '/home/calat/CLionProjects/particles/differ/explicit_gas_T=0.5_d2g=0.01_h=0.05_tau=0.005_K=0.01.dat' using 1:3 title "explicit" w l ls 4

unset format x
@BMARGIN; @LMARGIN
unset label 1
unset y2tics
unset y2label
#unset label 2
set xtics
#set ylabel 'Dust velocity'
set xlabel 'Distance'
#set key left top
#set label 1 'near, CFL=0.1' at graph 0.4,0.1
set ytics
#unset ylabel
plot './particles/differ/cells_dust_T=0_d2g=0.01_h=0.05_tau=0.005_K=0.01.dat' using 1:3 title "cells" w l ls 3, '/home/calat/CLionProjects/particles/differ/explicit_dust_T=0_d2g=0.01_h=0.05_tau=0.005_K=0.01.dat' using 1:3 title "explicit" w l ls 4

@BMARGIN; @RMARGIN
unset ytics
unset ylabel
#set y2label 'Gas density'
set y2tics mirror
plot './particles/differ/cells_gas_T=0_d2g=0.01_h=0.05_tau=0.005_K=0.01.dat' using 1:3 title "cells" w l ls 3, '/home/calat/CLionProjects/particles/differ/explicit_gas_T=0_d2g=0.01_h=0.05_tau=0.005_K=0.01.dat' using 1:3 title "explicit" w l ls 4

unset multiplot
